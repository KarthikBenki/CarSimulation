package org.tw;

public class TyreFlatException extends Exception {
    public TyreFlatException(String message) {
        super(message);
    }
}
