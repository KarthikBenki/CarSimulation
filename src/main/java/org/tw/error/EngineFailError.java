package org.tw.error;

public class EngineFailError extends Error {
    public EngineFailError(String message) {
        super(message);
    }
}
