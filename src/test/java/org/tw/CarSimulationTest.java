package org.tw;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.tw.error.EngineFailError;
import org.tw.exception.FuelEmptyException;

import static org.junit.jupiter.api.Assertions.*;


public class CarSimulationTest {
    public CarSimulation carSimulation;

    @BeforeEach
    void setUp() {
        carSimulation = new CarSimulation();
    }

    @Test
    void shouldBeAbleToDriveWhenEngineIsRunning() {
        assertDoesNotThrow(() -> carSimulation.drive());
    }

    @Test
    void shouldNotBeAbleToDriveWhenFuelTankIsEmpty() {
        carSimulation.setFuelTankEmpty(true);
        assertThrowsExactly(FuelEmptyException.class, () -> carSimulation.drive());
    }

    @Test
    void shouldNotBeAbleToDriveWhenTyreIsFlat() {
        carSimulation.setTyreFlat(true);
        assertThrowsExactly(TyreFlatException.class, () -> carSimulation.drive());
    }

    @Test
    void shouldNotBeAbleToDriveWhenEngineFails() {
        carSimulation.setEngineFail(true);
        assertThrowsExactly(EngineFailError.class, () -> carSimulation.drive());
    }
}
