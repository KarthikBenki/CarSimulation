package org.tw;

import org.tw.error.EngineFailError;
import org.tw.exception.FuelEmptyException;

public class CarSimulation {

    public void setEngineFail(boolean engineFail) {
        isEngineFail = engineFail;
    }

    private boolean isEngineFail;

    public void setTyreFlat(boolean tyreFlat) {
        isTyreFlat = tyreFlat;
    }

    private boolean isTyreFlat;

    public void setFuelTankEmpty(boolean fuelTankEmpty) {
        isFuelTankEmpty = fuelTankEmpty;
    }

    private boolean isFuelTankEmpty;

    public void drive() throws FuelEmptyException, TyreFlatException {
        if (isFuelTankEmpty) throw new FuelEmptyException("Fuel is empty please fill it soon");
        if (isTyreFlat) throw new TyreFlatException("Tyre is flat please fill the air");
        if (isEngineFail) throw new EngineFailError("Engine failed please repair");
    }


}
