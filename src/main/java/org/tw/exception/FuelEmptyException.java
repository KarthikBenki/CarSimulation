package org.tw.exception;

public class FuelEmptyException extends Exception {
    public FuelEmptyException(String message) {
        super(message);
    }
}
